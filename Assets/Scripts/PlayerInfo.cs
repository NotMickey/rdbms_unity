﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo
{
    public string PlayerName { get { return playerName; } set { playerName = value; } }

    public int Gold { get { return gold; } set { gold = value; } }

    public int Level { get { return level; } set { level = value; } }

    public float Experience { get { return experience; } set { experience = value; } }

    string playerName;
    int gold;
    int level;
    float experience;
}
