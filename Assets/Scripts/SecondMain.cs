﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondMain : MonoBehaviour {

    [SerializeField]
    InputField goldAmount;

    [SerializeField]
    Text output_text;

    public void GetPlayersWithGold()
    {
        int amount = 0;
        amount = int.Parse(goldAmount.text);

        List<PlayerInfo> infos = new List<PlayerInfo>();

        infos = SQLiteExample.Instance.GetAllPlayersWithGold(amount);

        if (infos.Count > 0)
        {
            output_text.text = "";

            foreach (PlayerInfo info in infos)
            {
                output_text.text += info.PlayerName + " " + info.Gold.ToString();

                output_text.text += "\n";
            }
        }
    }
}
