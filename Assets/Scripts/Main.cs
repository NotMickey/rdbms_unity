﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Main : MonoBehaviour {

    [SerializeField]
    private Text output_insert;

    [SerializeField]
    private InputField input_name;

    [SerializeField]
    private Text output_delete;

    public void Insert()
    {
        var random = new System.Random();

        string name = input_name.text;
        input_name.text = "";

        InsertPlayer(name, random.Next(40), random.Next(50), (float)random.Next(100));
    }

    public void InsertPlayer(string name, int gold, int level, float xp)
    {
        SQLiteExample.Instance.InsertPlayer(name, gold, level, xp);

        output_insert.text = "Inserted ( " + name + ", " + gold + ", " + level + ", " + xp + " )";
    }

    public void Delete()
    {
        string name = input_name.text;
        input_name.text = "";

        if (SQLiteExample.Instance.DeletePlayer(name))
        {
            output_delete.text = "DELETED " + name;
        }
        else
        {
            output_delete.text = "DOES NOT EXIST";
        }
    }
}
