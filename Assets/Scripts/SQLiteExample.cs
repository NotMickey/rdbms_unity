﻿using UnityEngine;
using System;
using System.Data;
using Mono.Data.SqliteClient;
using System.Collections.Generic;

public class SQLiteExample : MonoBehaviour
{
    public static SQLiteExample Instance { get { return _instance; } }
    public bool DebugMode = false;

    // Database location
    private static string _sqlDBLocation;

    // Database name
    private const string SQL_DB_NAME = "PlayerDatabase";

    // Player information table name
    private const string SQL_TABLE_NAME = "PlayerData";

    // Table columns
    private const string COL_NAME = "name";
    private const string COL_GOLD = "gold";
    private const string COL_EXP = "experience";
    private const string COL_LEVEL = "level";

    // Represents an open connection to a data source.
    private IDbConnection _connection = null;

    // Represents a statement that is executed while an open connection to a data source exists.
    private IDbCommand _command = null;

    // Provides a means of reading one or more forward-only streams of result sets obtained by executing a command at a data source
    private IDataReader _reader = null;

    // Query string
    private string _sqlString;

    private bool _createNewTable = false;
    private static SQLiteExample _instance;

    void Awake()
    {
        // Set the file location
        _sqlDBLocation = "URI=file:" + SQL_DB_NAME + ".db";
        //_sqlDBLocation = "URI=file:" + Application.persistentDataPath + "/" + SQL_DB_NAME + ".db";

        Debug.Log(_sqlDBLocation);

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
            DontDestroyOnLoad(this);
        }

        SQLiteInit();
    }

    /// <summary>
    /// Basic initialization of SQLite
    /// </summary>
    private void SQLiteInit()
    {
        Debug.Log("Opening SQLite Connection");
        _connection = new SqliteConnection(_sqlDBLocation);
        _command = _connection.CreateCommand();
        _connection.Open();

        #region speed

        // WAL = write ahead logging, very huge speed increase
        _command.CommandText = "PRAGMA journal_mode = WAL;";
        _command.ExecuteNonQuery();

        // journal mode = look it up on google, I don't remember
        _command.CommandText = "PRAGMA journal_mode";
        _reader = _command.ExecuteReader();
        _reader.Close();

        // more speed increases
        _command.CommandText = "PRAGMA synchronous = OFF";
        _command.ExecuteNonQuery();

        // and some more
        _command.CommandText = "PRAGMA synchronous";
        _reader = _command.ExecuteReader();
        _reader.Close();

        #endregion

        // here we check if the table you want to use exists or not.  If it doesn't exist we create it.
        _command.CommandText = "SELECT name FROM sqlite_master WHERE name='" + SQL_TABLE_NAME + "'";
        _reader = _command.ExecuteReader();
        if (!_reader.Read())
        {
            Debug.Log("SQLiter - Could not find SQLite table " + SQL_TABLE_NAME);
            _createNewTable = true;
        }
        _reader.Close();

        // create new table if it wasn't found
        if (_createNewTable)
        {
            Debug.Log("SQLiter - Creating new SQLite table " + SQL_TABLE_NAME);

            // insurance policy, drop table
            _command.CommandText = "DROP TABLE IF EXISTS " + SQL_TABLE_NAME;
            _command.ExecuteNonQuery();

            // create new - SQLite recommendation is to drop table, not clear it
            _sqlString = "CREATE TABLE IF NOT EXISTS " + SQL_TABLE_NAME + " (" +
                COL_NAME + " TEXT UNIQUE, " +
                COL_GOLD + " INTEGER, " +
                COL_LEVEL + " INTEGER, " +
                COL_EXP + " REAL)";
            _command.CommandText = _sqlString;
            _command.ExecuteNonQuery();
        }
        else
        {
            if (DebugMode)
                Debug.Log("SQLiter - SQLite table " + SQL_TABLE_NAME + " was found");
        }

        // close connection
        _connection.Close();
    }

    public void InsertPlayer(string name, int gold, int level, float xp)
    {
        name = name.ToLower();

        // note - this will replace any item that already exists, overwriting them.  
        // normal INSERT without the REPLACE will throw an error if an item already exists
        _sqlString = "INSERT OR REPLACE INTO " + SQL_TABLE_NAME
            + " ("
            + COL_NAME + ","
            + COL_GOLD + ","
            + COL_LEVEL + ","
            + COL_EXP
            + ") VALUES ('"
            + name + "',"  // note that string values need quote or double-quote delimiters
            + gold + ","
            + level + ","
            + xp
            + ");";

        if (DebugMode)
            Debug.Log(_sqlString);

        _connection.Open();
        _command.CommandText = _sqlString;
        _command.ExecuteNonQuery();
        _connection.Close();
    }

    public bool DeletePlayer(string nameKey)
    {
        nameKey = nameKey.ToLower();

        if (!ProfileExists(COL_NAME, nameKey))
        {
            return false;
        }

        _sqlString = "DELETE FROM " + SQL_TABLE_NAME + " WHERE " + COL_NAME + "='" + nameKey + "'";

        _connection.Open();
        _command.CommandText = _sqlString;
        _command.ExecuteNonQuery();
        _connection.Close();

        return true;
    }

    public List<PlayerInfo> GetAllPlayersWithGold(int gold)
    {
        List<PlayerInfo> players = new List<PlayerInfo>();

        _connection.Open();

        _command.CommandText = "SELECT " + COL_NAME + "," + COL_LEVEL + "," + COL_EXP + "," + COL_GOLD +" FROM " + SQL_TABLE_NAME
            + " WHERE " + COL_GOLD + " >= " + gold;

        _reader = _command.ExecuteReader();

        while (_reader.Read())
        {
            PlayerInfo info = new PlayerInfo();

            info.PlayerName = _reader.GetString(0);
            info.Level = _reader.GetInt32(1);
            info.Experience = _reader.GetFloat(2);
            info.Gold = _reader.GetInt32(3);

            players.Add(info);
        }

        _reader.Close();
        _connection.Close();

        return players;
    }

    private bool ProfileExists(string column, string value)
    {
        _connection.Open();
        _command.CommandText = "SELECT " + column + " FROM " + SQL_TABLE_NAME + " WHERE " + COL_NAME + "='" + value + "'";
        _reader = _command.ExecuteReader();

        if (_reader.Read())
        {
            _reader.Close();
            _connection.Close();
            return true;
        }

        _reader.Close();
        _connection.Close();

        return false;
    }

    void OnDestroy()
    {
        Debug.Log("Closing SQLite connection");
        SQLiteClose();
    }

    /// <summary>
    /// Basic execute command - open, create command, execute, close
    /// </summary>
    /// <param name="commandText"></param>
    public void ExecuteNonQuery(string commandText)
    {
        _connection.Open();
        _command.CommandText = commandText;
        _command.ExecuteNonQuery();
        _connection.Close();
    }

    /// <summary>
    /// Clean up everything for SQLite
    /// </summary>
    private void SQLiteClose()
    {
        if (_reader != null && !_reader.IsClosed)
            _reader.Close();
        _reader = null;

        if (_command != null)
            _command.Dispose();
        _command = null;

        if (_connection != null && _connection.State != ConnectionState.Closed)
            _connection.Close();
        _connection = null;
    }
}
